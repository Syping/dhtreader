#include <stdio.h>
#include <unistd.h>
#include <pi_2_dht_read.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_BBLUE   "\x1b[94m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int main(int argc, char argv[])
{
    float humid;
    float temp;
    while(1) {
        int result = pi_2_dht_read(22, 22, &humid, &temp);
        if (result != DHT_SUCCESS) {
            printf(ANSI_COLOR_RED "Error" ANSI_COLOR_RESET ": READING FAILED\n");
        }
        else {
            printf(ANSI_COLOR_BBLUE "Humidity" ANSI_COLOR_RESET ": %.1f%s\n", humid, "%");
            printf(ANSI_COLOR_GREEN "Temp" ANSI_COLOR_RESET ": %.1f%s\n", temp, "°C");
        }
        sleep(1);
    }
    return 0;
}
